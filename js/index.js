const grid = document.querySelector('.grid')
const startBtn = document.getElementById('start')
const score = document.getElementById('score')

let squares = []
let currentSnake = [2,1,0]
let direction = 1
let width = 10

function createGrid() {
    // create 100 of these elements
    for(var i = 0; i < 100; i++) {
        // create elements
        const square = document.createElement('div')
        // add styling to these elements
        square.classList.add('square')
        // create array of squares
        grid.appendChild(square)
        // push it into a new squares array
        squares.push(square)
    }
}

createGrid()

currentSnake.forEach(index => squares[index].classList.add('snake'))

function move() {
    // Disable move as snake hits any wall.
    if (
        (currentSnake[0] + width >= width*width && direction === width) ||
        (currentSnake[0] % width === 9 && direction === 1) ||
        (currentSnake[0] % width === 0 && direction === -1) ||
        (currentSnake[0] - width < 0 && direction === -width) ||
        squares[currentSnake[0] + direction].classList.contains('snake')
    ) {
       return clearInterval(timerId)
    }

    // Remove last element from our currentSnake array
    const tail = currentSnake.pop()
    // Remove styling from last element
    squares[tail].classList.remove('snake')
    // Add square in direction we are heading
    currentSnake.unshift(currentSnake[0]+direction)
    // add styling so we can see it
    squares[currentSnake[0]].classList.add('snake')
    
}

move()
// Interval for move function
let timerId = setInterval(move, 1000);

/* 

Key codes 

    39 -- right arrow
    38 -- up arrow
    37 -- left arrow
    40 -- down arrow

*/

function control(e) {
    if(e.keyCode === 39) {
        console.log('right pressed')
        direction = 1
    } else if (e.keyCode === 38) {
        console.log('up pressed')
        direction = -width
    } else if (e.keyCode === 37) {
        console.log('left pressed')
        direction = -1
    } else if (e.keyCode === 40) {
        console.log('down pressed')
        direction = width
    }
}
document.addEventListener('keyup', control)







